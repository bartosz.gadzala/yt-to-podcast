#!/bin/bash

series_dir="$1"
list_file="${series_dir}/list.csv"

[[ -z "${series_dir}" ]] && exit 1
[[ ! -d "${series_dir}" ]] && exit 2
[[ ! -f "${list_file}" ]] && exit 2

echo "Cleaning up orphaned tracks in ${series_dir}..."

for track_file in $(ls ${series_dir}/*.mp3); do
	if [[ -f "${track_file}.info.json" ]]; then
		webpage_url=$(cat "${track_file}.info.json" | jq -r '.webpage_url')
		if [[ -z "${webpage_url}" ]]; then
			echo "Undefined webpage_url in ${track_file}.info.json, removing ${track_file}"	
			rm "${track_file}"
			rm "${track_file}.info.json"
		elif ! grep --silent "${webpage_url}" "${list_file}"; then
			echo "URL ${webpage_url} no longer in ${list_file}, removing ${track_file}"
			rm "${track_file}"
			rm "${track_file}.info.json"
#		else
#			echo "URL ${webpage_url} still used in ${list_file}, keeping ${track_file}"
		fi
	else
		echo "Missing ${track_file}.info.json, removing ${track_file}"
		rm "${track_file}"
	fi
done