FROM debian:sid-slim

VOLUME [ "/data" ]
WORKDIR /data

EXPOSE 8080

RUN apt-get update -y && apt-get upgrade -y 
RUN apt-get install -y --fix-missing ca-certificates locales locales-all python3 python3-pip zip atomicparsley git make vim bash jq nginx aria2 ffmpeg
RUN apt-get autoremove

RUN aria2c https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp --dir /usr/local/bin -o yt-dlp
RUN chmod a+rx /usr/local/bin/yt-dlp  # Make executable

RUN mkdir -p /opt/yt-to-podcast/
ADD cleanup.sh /opt/yt-to-podcast/cleanup.sh
ADD download_tracks.sh /opt/yt-to-podcast/download_tracks.sh
ADD recreate_feed_xml.sh /opt/yt-to-podcast/recreate_feed_xml.sh
ADD entrypoint.sh /opt/yt-to-podcast/entrypoint.sh
ADD nginx.conf /opt/yt-to-podcast/nginx.conf

ENTRYPOINT [ "/opt/yt-to-podcast/entrypoint.sh" ]
