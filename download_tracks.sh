#!/bin/bash

channel_name="$1"
series_name="$2"
series_dir="$3"
list_file="${series_dir}/list.csv"

[[ -z "${channel_name}" ]] && exit 1
[[ -z "${series_name}" ]] && exit 1
[[ -z "${series_dir}" ]] && exit 1
[[ ! -d "${series_dir}" ]] && exit 2
[[ ! -f "${list_file}" ]] && exit 2

echo "Downloading tracks for ${channel_name} / ${series_name}..."

function to_filename() {
    echo "$1" | sed -e 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZĄĆĘŁŃÓŚŹŻąćęłńóśźż/abcdefghijklmnopqrstuvwxyzacelnoszzacelnoszz/' -e 's/ /_/g' | tr -c -d '[:alnum:]_'
}

while IFS="," read -r title episode url
do
    audio_file="$(to_filename "${series_name}")-$(to_filename "${episode}")-$(to_filename "${title}").mp3"

    echo "========================================================================="
    echo "Channel: ${channel_name}"
    echo "Title:   ${series_name} #${episode}: ${title}"
    echo "File:    ${audio_file}"

    if [[ -f "${series_dir}/${audio_file}" ]]; then
        echo "Already dowloaded"
    else
        echo "Downloading ${url} as ${audio_file}..."
        /usr/local/bin/yt-dlp -U --downloader=aria2c \
            --downloader-args 'aria2c:--min-split-size=1M --max-connection-per-server=16 --max-concurrent-downloads=16 --split=16' \
            --extract-audio --audio-format mp3 --audio-quality 9 \
            --write-info-json \
            -o "${series_dir}/${audio_file}" "${url}"
    fi
done < ${list_file}
