#!/bin/bash

echo "Starting nginx server"
nginx -c /opt/yt-to-podcast/nginx.conf

channels_dir=/data

while true; do
    echo "<html>" > "${channels_dir}/index.html"
    echo "<head>" >> "${channels_dir}/index.html"
    echo "<title>Podcast feeds</title>" >> "${channels_dir}/index.html"
    echo "</head>" >> "${channels_dir}/index.html"
    echo "<body>" >> "${channels_dir}/index.html"

    for channel_dir in $(ls "${channels_dir}"); do
        [[ ! -d "${channels_dir}/${channel_dir}" ]] && continue
        [[ ! -f "${channels_dir}/${channel_dir}/metadata" ]] && continue

        channel_name="$(cat ${channels_dir}/${channel_dir}/metadata)"
        
        echo "Processing ${channel_name} channel..."

        echo "<h1>${channel_name}</h1>" >> "${channels_dir}/index.html"

        for series_dir in $(ls "${channels_dir}/${channel_dir}"); do
            [[ ! -d "${channels_dir}/${channel_dir}/${series_dir}" ]] && continue
            [[ ! -f "${channels_dir}/${channel_dir}/${series_dir}/metadata" ]] && continue
            [[ ! -f "${channels_dir}/${channel_dir}/${series_dir}/list.csv" ]] && continue

            series_name="$(cat ${channels_dir}/${channel_dir}/${series_dir}/metadata)"

            echo "Processing ${series_name} series..."

            echo "<h2>${series_name}</h2>" >> "${channels_dir}/index.html"
            echo "<a href=\"${channel_dir}/${series_dir}/feed.xml\">${BASE_URL}${channel_dir}/${series_dir}/feed.xml</a>" >> "${channels_dir}/index.html"

            /opt/yt-to-podcast/download_tracks.sh   "${channel_name}" "${series_name}" "${channels_dir}/${channel_dir}/${series_dir}"
            /opt/yt-to-podcast/recreate_feed_xml.sh "${channel_name}" "${series_name}" "${channels_dir}/${channel_dir}/${series_dir}" "${channel_dir}/${series_dir}"
            /opt/yt-to-podcast/cleanup.sh                                              "${channels_dir}/${channel_dir}/${series_dir}"
        done
    done

    echo "</body>" >> "${channels_dir}/index.html"
    echo "</html>" >> "${channels_dir}/index.html"

    echo "Zzzzz..."
    sleep 60
done