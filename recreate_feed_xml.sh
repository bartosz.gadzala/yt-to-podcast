#!/bin/bash

channel_name="$1"
series_name="$2"
series_dir="$3"
path="$4"
list_file="${series_dir}/list.csv"
feed_xml_file="${series_dir}/feed.xml"

[[ -z "${channel_name}" ]] && exit 1
[[ -z "${series_name}" ]] && exit 1
[[ -z "${series_dir}" ]] && exit 1
[[ ! -d "${series_dir}" ]] && exit 2
[[ ! -f "${list_file}" ]] && exit 2

echo "Recreating feed.xml for ${channel_name} / ${series_name}..."

function append() {
    echo "$*" >> "${feed_xml_file}"
}

function to_filename() {
    echo "$1" | sed -e 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZĄĆĘŁŃÓŚŹŻąćęłńóśźż/abcdefghijklmnopqrstuvwxyzacelnoszzacelnoszz/' -e 's/ /_/g' | tr -c -d '[:alnum:]_'
}

echo '<?xml version="1.0" encoding="UTF-8"?>' > "${feed_xml_file}"
append '<rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">'
append '<channel>'
append "<title>${channel_name} - ${series_name}</title>"
append '<language>pl-pl</language>'
append "<link>${BASE_URL}${path}</link>"
append '<itunes:owner>'
append "<itunes:email>${OWNER_EMAIL}</itunes:email>"
append "<itunes:name>${OWNER_NAME}</itunes:name>"
append '</itunes:owner>'
append '<description>My favourites sideloaded podcasts.</description>'
append '<itunes:summary>My favourites sideloaded podcasts.</itunes:summary>'
append '<itunes:explicit>no</itunes:explicit>'
append '<itunes:type>Serial</itunes:type>';

while IFS="," read -r title episode url
do
	audio_file="$(to_filename "${series_name}")-$(to_filename "${episode}")-$(to_filename "${title}").mp3"

	if [[ -f "${series_dir}/${audio_file}" ]]; then
    	link="${BASE_URL}${path}/${audio_file}"
    	file_size=$(ls -nl ${series_dir}/${audio_file} | awk '{print $5}')
		duration=$(cat "${series_dir}/${audio_file}.info.json" | jq -r '.duration')
		if [[ -z "${duration}" ]]; then
			duration=$(ffprobe -show_streams -print_format json "${series_dir}/${audio_file}" -v fatal | jq -r '.streams[0].duration | tonumber | floor')
		fi
		release_date=$(cat "${series_dir}/${audio_file}.info.json" | jq -r '.upload_date')
		description=$(cat "${series_dir}/${audio_file}.info.json" | jq -r '.description' | awk -v RS="\r?\n\r?\n" '{print $0;exit}')
		thumbnail=$(cat "${series_dir}/${audio_file}.info.json" | jq -r '.thumbnails[] | select(((.url|endswith("jpg")) or (.url|endswith("png"))) and (.height>600)) | .url')

		echo "========================================================================="
		echo "Author:      ${channel_name}"
		echo "Title:       ${series_name} #${episode}: ${title}"
		echo "Date:        ${release_date}"
		echo "URL:         ${url}"
		echo "File:        ${series_dir}/${audio_file}"    
		echo "Duration:    ${duration}s"
		echo "Description: ${description}"

    	append '<item>'
    	append "<link>${url}</link>"
    	append "<title>${title}</title>"
		append "<itunes:episode>${episode}</itunes:episode>"
		append "<description>${description}</description>"
    	append "<itunes:author>${channel_name}</itunes:author>"
    	append "<guid isPermaLink='false'>${link}</guid>"
    	append "<enclosure url='${link}' length='${file_size}' type='audio/mpeg' />"
    	append "<itunes:duration>${duration}</itunes:duration>"
		append "<pubDate>$(date -d "${release_date}" -R)</pubDate>"
		append '<itunes:explicit>no</itunes:explicit>'
		[[ -n "${thumbnail}" ]] && append "<itunes:image>${thumbnail}</itunes:image>"
    	append '</item>'
    else
		echo "File ${audio_file} is missing. Skipping"
    fi
done < ${list_file}

append '</channel>'
append "</rss>"
